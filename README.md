# dyff-workflows-sink deployment

<!-- BADGIE TIME -->

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)

<!-- END BADGIE TIME -->

workflows-sink deployments for the dyff-cloud cluster.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Bootstrap procedure

### Prerequisites

### Steps

- Create a service account for each environment.

- Grant the following permissions to each service account:

  - `Compute Network Viewer`

  - `Kubernetes Engine Developer`

- Create service account API keys and save each one as `credentials.json` in the
  corresponding workspace directory.

## Development

This project is configured to use the GitLab-managed Terraform state backend.

To connect to the state for local development, run the following:

```bash
cd deploy/production
terraform init -upgrade -backend-config="username=USERNAME" -backend-config="password=PASSWORD"
```

<!-- prettier-ignore-start -->
<!-- prettier-ignore-end -->

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
