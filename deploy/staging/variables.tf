# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "google_cloud_service_account_file" {
  type    = string
  default = "credentials.json"
}

variable "remote_state_user" {
  type = string
}

variable "remote_state_password" {
  type      = string
  sensitive = true
}
