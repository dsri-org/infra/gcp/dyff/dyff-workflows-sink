# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "workflows_sink" {
  metadata {
    name = "workflows-sink"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/workflows-sink/workflows-sink
resource "helm_release" "workflows_sink_mongodb" {
  name       = "workflows-sink-mongodb"
  namespace  = kubernetes_namespace.workflows_sink.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  chart      = "workflows-sink"
  version    = "0.7.1"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    replicaCount = 1

    extraEnvVarsConfigMap = {
      DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS  = local.kafka.bootstrap_servers
      DYFF_KAFKA__CONFIG__GROUP_ID           = "dyff.system.mongodb.sink"
      DYFF_KAFKA__TOPICS__WORKFLOWS_STATE    = local.kafka.topics_map["dyff.workflows.state"].name
      DYFF_WORKFLOWS_SINK__SINK_KIND         = "mongodb"
      DYFF_WORKFLOWS_SINK__MONGODB__DATABASE = local.mongodb.credentials["command"].database

      DYFF_RESOURCES__SAFETYCASES__STORAGE__URL = "${local.storage.buckets["safetycases"].s3_url}"

      DYFF_STORAGE__BACKEND        = "dyff.storage.backend.s3.storage.S3StorageBackend"
      DYFF_STORAGE__S3__ENDPOINT   = local.storage.hostname
      DYFF_STORAGE__S3__ACCESS_KEY = local.storage.access_id
    }

    extraEnvVarsSecret = {}

    resources = {
      limits = {
        cpu                 = "750m"
        memory              = "768Mi"
        "ephemeral-storage" = "1024Mi"
      }
      requests = {
        cpu                 = "500m"
        memory              = "512Mi"
        "ephemeral-storage" = "50Mi"
      }
    }

  })]

  set_sensitive {
    name  = "extraEnvVarsSecret.DYFF_WORKFLOWS_SINK__MONGODB__CONNECTION_STRING"
    value = local.mongodb.credentials["command"].connection_string_escaped
  }

  set_sensitive {
    name  = "extraEnvVarsSecret.DYFF_STORAGE__S3__SECRET_KEY"
    value = local.storage.secret
  }
}

# https://artifacthub.io/packages/helm/workflows-sink/workflows-sink
resource "helm_release" "workflows_sink_backup" {
  name       = "workflows-sink-backup-s3"
  namespace  = kubernetes_namespace.workflows_sink.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  chart      = "workflows-sink"
  version    = "0.7.1"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    replicaCount = 1

    extraEnvVarsConfigMap = {
      DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS = local.kafka.bootstrap_servers
      DYFF_KAFKA__CONFIG__GROUP_ID          = "dyff.system.backup.workflows"
      DYFF_KAFKA__TOPICS__WORKFLOWS_STATE   = local.kafka.topics_map["dyff.workflows.state"].name
      DYFF_WORKFLOWS_SINK__SINK_KIND        = "s3"
      DYFF_WORKFLOWS_SINK__S3__STORAGE_PATH = "${local.storage.buckets["backup"].s3_url}/kafka"

      DYFF_STORAGE__BACKEND        = "dyff.storage.backend.s3.storage.S3StorageBackend"
      DYFF_STORAGE__S3__ENDPOINT   = local.storage.hostname
      DYFF_STORAGE__S3__ACCESS_KEY = local.storage.access_id
    }

    resources = {
      limits = {
        cpu                 = "750m"
        memory              = "768Mi"
        "ephemeral-storage" = "1024Mi"
      }
      requests = {
        cpu                 = "500m"
        memory              = "512Mi"
        "ephemeral-storage" = "50Mi"
      }
    }
  })]

  set_sensitive {
    name  = "extraEnvVarsSecret.DYFF_STORAGE__S3__SECRET_KEY"
    value = local.storage.secret
  }
}
